package mesh.cpn.idl.internal_idl.generated.resource;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: resource/static.proto")
public final class StaticResourceServiceGrpc {

  private StaticResourceServiceGrpc() {}

  public static final String SERVICE_NAME = "resource.StaticResourceService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest,
      mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse> getStaticResourceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "StaticResource",
      requestType = mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest.class,
      responseType = mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest,
      mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse> getStaticResourceMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest, mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse> getStaticResourceMethod;
    if ((getStaticResourceMethod = StaticResourceServiceGrpc.getStaticResourceMethod) == null) {
      synchronized (StaticResourceServiceGrpc.class) {
        if ((getStaticResourceMethod = StaticResourceServiceGrpc.getStaticResourceMethod) == null) {
          StaticResourceServiceGrpc.getStaticResourceMethod = getStaticResourceMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest, mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "resource.StaticResourceService", "StaticResource"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new StaticResourceServiceMethodDescriptorSupplier("StaticResource"))
                  .build();
          }
        }
     }
     return getStaticResourceMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static StaticResourceServiceStub newStub(io.grpc.Channel channel) {
    return new StaticResourceServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static StaticResourceServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new StaticResourceServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static StaticResourceServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new StaticResourceServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class StaticResourceServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void staticResource(mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getStaticResourceMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getStaticResourceMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest,
                mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse>(
                  this, METHODID_STATIC_RESOURCE)))
          .build();
    }
  }

  /**
   */
  public static final class StaticResourceServiceStub extends io.grpc.stub.AbstractStub<StaticResourceServiceStub> {
    private StaticResourceServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StaticResourceServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StaticResourceServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StaticResourceServiceStub(channel, callOptions);
    }

    /**
     */
    public void staticResource(mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getStaticResourceMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class StaticResourceServiceBlockingStub extends io.grpc.stub.AbstractStub<StaticResourceServiceBlockingStub> {
    private StaticResourceServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StaticResourceServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StaticResourceServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StaticResourceServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse staticResource(mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest request) {
      return blockingUnaryCall(
          getChannel(), getStaticResourceMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class StaticResourceServiceFutureStub extends io.grpc.stub.AbstractStub<StaticResourceServiceFutureStub> {
    private StaticResourceServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StaticResourceServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StaticResourceServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StaticResourceServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse> staticResource(
        mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getStaticResourceMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_STATIC_RESOURCE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final StaticResourceServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(StaticResourceServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_STATIC_RESOURCE:
          serviceImpl.staticResource((mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceRequest) request,
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.resource.Static.StaticResourceResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class StaticResourceServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    StaticResourceServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return mesh.cpn.idl.internal_idl.generated.resource.Static.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("StaticResourceService");
    }
  }

  private static final class StaticResourceServiceFileDescriptorSupplier
      extends StaticResourceServiceBaseDescriptorSupplier {
    StaticResourceServiceFileDescriptorSupplier() {}
  }

  private static final class StaticResourceServiceMethodDescriptorSupplier
      extends StaticResourceServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    StaticResourceServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (StaticResourceServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new StaticResourceServiceFileDescriptorSupplier())
              .addMethod(getStaticResourceMethod())
              .build();
        }
      }
    }
    return result;
  }
}
