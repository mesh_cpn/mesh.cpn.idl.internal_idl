package mesh.cpn.idl.internal_idl.generated.ai.object_detect;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: ai/object_detect/object_detect.proto")
public final class DetectObjectServiceGrpc {

  private DetectObjectServiceGrpc() {}

  public static final String SERVICE_NAME = "object_detect.DetectObjectService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest,
      mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse> getDetectObjectMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DetectObject",
      requestType = mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest.class,
      responseType = mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest,
      mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse> getDetectObjectMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest, mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse> getDetectObjectMethod;
    if ((getDetectObjectMethod = DetectObjectServiceGrpc.getDetectObjectMethod) == null) {
      synchronized (DetectObjectServiceGrpc.class) {
        if ((getDetectObjectMethod = DetectObjectServiceGrpc.getDetectObjectMethod) == null) {
          DetectObjectServiceGrpc.getDetectObjectMethod = getDetectObjectMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest, mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "object_detect.DetectObjectService", "DetectObject"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new DetectObjectServiceMethodDescriptorSupplier("DetectObject"))
                  .build();
          }
        }
     }
     return getDetectObjectMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DetectObjectServiceStub newStub(io.grpc.Channel channel) {
    return new DetectObjectServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DetectObjectServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DetectObjectServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DetectObjectServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DetectObjectServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class DetectObjectServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void detectObject(mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDetectObjectMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getDetectObjectMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest,
                mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse>(
                  this, METHODID_DETECT_OBJECT)))
          .build();
    }
  }

  /**
   */
  public static final class DetectObjectServiceStub extends io.grpc.stub.AbstractStub<DetectObjectServiceStub> {
    private DetectObjectServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DetectObjectServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DetectObjectServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DetectObjectServiceStub(channel, callOptions);
    }

    /**
     */
    public void detectObject(mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDetectObjectMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DetectObjectServiceBlockingStub extends io.grpc.stub.AbstractStub<DetectObjectServiceBlockingStub> {
    private DetectObjectServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DetectObjectServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DetectObjectServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DetectObjectServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse detectObject(mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest request) {
      return blockingUnaryCall(
          getChannel(), getDetectObjectMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DetectObjectServiceFutureStub extends io.grpc.stub.AbstractStub<DetectObjectServiceFutureStub> {
    private DetectObjectServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DetectObjectServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DetectObjectServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DetectObjectServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse> detectObject(
        mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDetectObjectMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_DETECT_OBJECT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DetectObjectServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DetectObjectServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DETECT_OBJECT:
          serviceImpl.detectObject((mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectRequest) request,
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.DetectResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DetectObjectServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DetectObjectServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return mesh.cpn.idl.internal_idl.generated.ai.object_detect.ObjectDetect.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("DetectObjectService");
    }
  }

  private static final class DetectObjectServiceFileDescriptorSupplier
      extends DetectObjectServiceBaseDescriptorSupplier {
    DetectObjectServiceFileDescriptorSupplier() {}
  }

  private static final class DetectObjectServiceMethodDescriptorSupplier
      extends DetectObjectServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DetectObjectServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DetectObjectServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DetectObjectServiceFileDescriptorSupplier())
              .addMethod(getDetectObjectMethod())
              .build();
        }
      }
    }
    return result;
  }
}
