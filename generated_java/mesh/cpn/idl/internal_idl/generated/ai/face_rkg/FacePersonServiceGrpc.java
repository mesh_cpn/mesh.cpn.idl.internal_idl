package mesh.cpn.idl.internal_idl.generated.ai.face_rkg;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: ai/face_rkg/face_rkg_person.proto")
public final class FacePersonServiceGrpc {

  private FacePersonServiceGrpc() {}

  public static final String SERVICE_NAME = "face_rkg.FacePersonService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getInsertMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Insert",
      requestType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest.class,
      responseType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getInsertMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getInsertMethod;
    if ((getInsertMethod = FacePersonServiceGrpc.getInsertMethod) == null) {
      synchronized (FacePersonServiceGrpc.class) {
        if ((getInsertMethod = FacePersonServiceGrpc.getInsertMethod) == null) {
          FacePersonServiceGrpc.getInsertMethod = getInsertMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "face_rkg.FacePersonService", "Insert"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new FacePersonServiceMethodDescriptorSupplier("Insert"))
                  .build();
          }
        }
     }
     return getInsertMethod;
  }

  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getUpdateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Update",
      requestType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest.class,
      responseType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getUpdateMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getUpdateMethod;
    if ((getUpdateMethod = FacePersonServiceGrpc.getUpdateMethod) == null) {
      synchronized (FacePersonServiceGrpc.class) {
        if ((getUpdateMethod = FacePersonServiceGrpc.getUpdateMethod) == null) {
          FacePersonServiceGrpc.getUpdateMethod = getUpdateMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "face_rkg.FacePersonService", "Update"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new FacePersonServiceMethodDescriptorSupplier("Update"))
                  .build();
          }
        }
     }
     return getUpdateMethod;
  }

  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getDeleteByIDMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DeleteByID",
      requestType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest.class,
      responseType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getDeleteByIDMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> getDeleteByIDMethod;
    if ((getDeleteByIDMethod = FacePersonServiceGrpc.getDeleteByIDMethod) == null) {
      synchronized (FacePersonServiceGrpc.class) {
        if ((getDeleteByIDMethod = FacePersonServiceGrpc.getDeleteByIDMethod) == null) {
          FacePersonServiceGrpc.getDeleteByIDMethod = getDeleteByIDMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "face_rkg.FacePersonService", "DeleteByID"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new FacePersonServiceMethodDescriptorSupplier("DeleteByID"))
                  .build();
          }
        }
     }
     return getDeleteByIDMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static FacePersonServiceStub newStub(io.grpc.Channel channel) {
    return new FacePersonServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static FacePersonServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new FacePersonServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static FacePersonServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new FacePersonServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class FacePersonServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void insert(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getInsertMethod(), responseObserver);
    }

    /**
     */
    public void update(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateMethod(), responseObserver);
    }

    /**
     */
    public void deleteByID(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteByIDMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getInsertMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest,
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>(
                  this, METHODID_INSERT)))
          .addMethod(
            getUpdateMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest,
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>(
                  this, METHODID_UPDATE)))
          .addMethod(
            getDeleteByIDMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest,
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>(
                  this, METHODID_DELETE_BY_ID)))
          .build();
    }
  }

  /**
   */
  public static final class FacePersonServiceStub extends io.grpc.stub.AbstractStub<FacePersonServiceStub> {
    private FacePersonServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FacePersonServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FacePersonServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FacePersonServiceStub(channel, callOptions);
    }

    /**
     */
    public void insert(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getInsertMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void update(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteByID(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteByIDMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class FacePersonServiceBlockingStub extends io.grpc.stub.AbstractStub<FacePersonServiceBlockingStub> {
    private FacePersonServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FacePersonServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FacePersonServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FacePersonServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse insert(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request) {
      return blockingUnaryCall(
          getChannel(), getInsertMethod(), getCallOptions(), request);
    }

    /**
     */
    public mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse update(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateMethod(), getCallOptions(), request);
    }

    /**
     */
    public mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse deleteByID(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest request) {
      return blockingUnaryCall(
          getChannel(), getDeleteByIDMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class FacePersonServiceFutureStub extends io.grpc.stub.AbstractStub<FacePersonServiceFutureStub> {
    private FacePersonServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FacePersonServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FacePersonServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FacePersonServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> insert(
        mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getInsertMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> update(
        mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse> deleteByID(
        mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteByIDMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_INSERT = 0;
  private static final int METHODID_UPDATE = 1;
  private static final int METHODID_DELETE_BY_ID = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final FacePersonServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(FacePersonServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_INSERT:
          serviceImpl.insert((mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest) request,
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>) responseObserver);
          break;
        case METHODID_UPDATE:
          serviceImpl.update((mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.UpdatePersonRequest) request,
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>) responseObserver);
          break;
        case METHODID_DELETE_BY_ID:
          serviceImpl.deleteByID((mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.DeletePersonRequest) request,
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.CurdPersonResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class FacePersonServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    FacePersonServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgPerson.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("FacePersonService");
    }
  }

  private static final class FacePersonServiceFileDescriptorSupplier
      extends FacePersonServiceBaseDescriptorSupplier {
    FacePersonServiceFileDescriptorSupplier() {}
  }

  private static final class FacePersonServiceMethodDescriptorSupplier
      extends FacePersonServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    FacePersonServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (FacePersonServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new FacePersonServiceFileDescriptorSupplier())
              .addMethod(getInsertMethod())
              .addMethod(getUpdateMethod())
              .addMethod(getDeleteByIDMethod())
              .build();
        }
      }
    }
    return result;
  }
}
