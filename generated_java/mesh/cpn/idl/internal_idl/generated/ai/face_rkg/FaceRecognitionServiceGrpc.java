package mesh.cpn.idl.internal_idl.generated.ai.face_rkg;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: ai/face_rkg/face_rkg.proto")
public final class FaceRecognitionServiceGrpc {

  private FaceRecognitionServiceGrpc() {}

  public static final String SERVICE_NAME = "face_rkg.FaceRecognitionService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse> getFaceRecognizeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "FaceRecognize",
      requestType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest.class,
      responseType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse> getFaceRecognizeMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse> getFaceRecognizeMethod;
    if ((getFaceRecognizeMethod = FaceRecognitionServiceGrpc.getFaceRecognizeMethod) == null) {
      synchronized (FaceRecognitionServiceGrpc.class) {
        if ((getFaceRecognizeMethod = FaceRecognitionServiceGrpc.getFaceRecognizeMethod) == null) {
          FaceRecognitionServiceGrpc.getFaceRecognizeMethod = getFaceRecognizeMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "face_rkg.FaceRecognitionService", "FaceRecognize"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new FaceRecognitionServiceMethodDescriptorSupplier("FaceRecognize"))
                  .build();
          }
        }
     }
     return getFaceRecognizeMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static FaceRecognitionServiceStub newStub(io.grpc.Channel channel) {
    return new FaceRecognitionServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static FaceRecognitionServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new FaceRecognitionServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static FaceRecognitionServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new FaceRecognitionServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class FaceRecognitionServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void faceRecognize(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getFaceRecognizeMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getFaceRecognizeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest,
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse>(
                  this, METHODID_FACE_RECOGNIZE)))
          .build();
    }
  }

  /**
   */
  public static final class FaceRecognitionServiceStub extends io.grpc.stub.AbstractStub<FaceRecognitionServiceStub> {
    private FaceRecognitionServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionServiceStub(channel, callOptions);
    }

    /**
     */
    public void faceRecognize(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getFaceRecognizeMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class FaceRecognitionServiceBlockingStub extends io.grpc.stub.AbstractStub<FaceRecognitionServiceBlockingStub> {
    private FaceRecognitionServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse faceRecognize(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest request) {
      return blockingUnaryCall(
          getChannel(), getFaceRecognizeMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class FaceRecognitionServiceFutureStub extends io.grpc.stub.AbstractStub<FaceRecognitionServiceFutureStub> {
    private FaceRecognitionServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse> faceRecognize(
        mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getFaceRecognizeMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_FACE_RECOGNIZE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final FaceRecognitionServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(FaceRecognitionServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_FACE_RECOGNIZE:
          serviceImpl.faceRecognize((mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeRequest) request,
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.RecognizeResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class FaceRecognitionServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    FaceRecognitionServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkg.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("FaceRecognitionService");
    }
  }

  private static final class FaceRecognitionServiceFileDescriptorSupplier
      extends FaceRecognitionServiceBaseDescriptorSupplier {
    FaceRecognitionServiceFileDescriptorSupplier() {}
  }

  private static final class FaceRecognitionServiceMethodDescriptorSupplier
      extends FaceRecognitionServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    FaceRecognitionServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (FaceRecognitionServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new FaceRecognitionServiceFileDescriptorSupplier())
              .addMethod(getFaceRecognizeMethod())
              .build();
        }
      }
    }
    return result;
  }
}
