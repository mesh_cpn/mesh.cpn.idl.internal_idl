package mesh.cpn.idl.internal_idl.generated.ai.face_rkg;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * 人脸识别监控服务 (C/S反转，人脸识别作为 client 向监控方流式传输监控数据)
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: ai/face_rkg/face_rkg_monitor.proto")
public final class FaceRecognitionMonitorServiceGrpc {

  private FaceRecognitionMonitorServiceGrpc() {}

  public static final String SERVICE_NAME = "face_rkg.FaceRecognitionMonitorService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse> getMonitorMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Monitor",
      requestType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame.class,
      responseType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse> getMonitorMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse> getMonitorMethod;
    if ((getMonitorMethod = FaceRecognitionMonitorServiceGrpc.getMonitorMethod) == null) {
      synchronized (FaceRecognitionMonitorServiceGrpc.class) {
        if ((getMonitorMethod = FaceRecognitionMonitorServiceGrpc.getMonitorMethod) == null) {
          FaceRecognitionMonitorServiceGrpc.getMonitorMethod = getMonitorMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "face_rkg.FaceRecognitionMonitorService", "Monitor"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new FaceRecognitionMonitorServiceMethodDescriptorSupplier("Monitor"))
                  .build();
          }
        }
     }
     return getMonitorMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static FaceRecognitionMonitorServiceStub newStub(io.grpc.Channel channel) {
    return new FaceRecognitionMonitorServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static FaceRecognitionMonitorServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new FaceRecognitionMonitorServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static FaceRecognitionMonitorServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new FaceRecognitionMonitorServiceFutureStub(channel);
  }

  /**
   * <pre>
   * 人脸识别监控服务 (C/S反转，人脸识别作为 client 向监控方流式传输监控数据)
   * </pre>
   */
  public static abstract class FaceRecognitionMonitorServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame> monitor(
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(getMonitorMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getMonitorMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame,
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse>(
                  this, METHODID_MONITOR)))
          .build();
    }
  }

  /**
   * <pre>
   * 人脸识别监控服务 (C/S反转，人脸识别作为 client 向监控方流式传输监控数据)
   * </pre>
   */
  public static final class FaceRecognitionMonitorServiceStub extends io.grpc.stub.AbstractStub<FaceRecognitionMonitorServiceStub> {
    private FaceRecognitionMonitorServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionMonitorServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionMonitorServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionMonitorServiceStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorFrame> monitor(
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getMonitorMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * 人脸识别监控服务 (C/S反转，人脸识别作为 client 向监控方流式传输监控数据)
   * </pre>
   */
  public static final class FaceRecognitionMonitorServiceBlockingStub extends io.grpc.stub.AbstractStub<FaceRecognitionMonitorServiceBlockingStub> {
    private FaceRecognitionMonitorServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionMonitorServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionMonitorServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionMonitorServiceBlockingStub(channel, callOptions);
    }
  }

  /**
   * <pre>
   * 人脸识别监控服务 (C/S反转，人脸识别作为 client 向监控方流式传输监控数据)
   * </pre>
   */
  public static final class FaceRecognitionMonitorServiceFutureStub extends io.grpc.stub.AbstractStub<FaceRecognitionMonitorServiceFutureStub> {
    private FaceRecognitionMonitorServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionMonitorServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionMonitorServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionMonitorServiceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_MONITOR = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final FaceRecognitionMonitorServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(FaceRecognitionMonitorServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_MONITOR:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.monitor(
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.MonitorResponse>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class FaceRecognitionMonitorServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    FaceRecognitionMonitorServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgMonitor.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("FaceRecognitionMonitorService");
    }
  }

  private static final class FaceRecognitionMonitorServiceFileDescriptorSupplier
      extends FaceRecognitionMonitorServiceBaseDescriptorSupplier {
    FaceRecognitionMonitorServiceFileDescriptorSupplier() {}
  }

  private static final class FaceRecognitionMonitorServiceMethodDescriptorSupplier
      extends FaceRecognitionMonitorServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    FaceRecognitionMonitorServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (FaceRecognitionMonitorServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new FaceRecognitionMonitorServiceFileDescriptorSupplier())
              .addMethod(getMonitorMethod())
              .build();
        }
      }
    }
    return result;
  }
}
