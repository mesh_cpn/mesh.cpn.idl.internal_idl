package mesh.cpn.idl.internal_idl.generated.ai.face_rkg;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: ai/face_rkg/face_rkg_query.proto")
public final class FaceRecognitionQueryServiceGrpc {

  private FaceRecognitionQueryServiceGrpc() {}

  public static final String SERVICE_NAME = "face_rkg.FaceRecognitionQueryService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse> getQueryByIDMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "QueryByID",
      requestType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam.class,
      responseType = mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam,
      mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse> getQueryByIDMethod() {
    io.grpc.MethodDescriptor<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse> getQueryByIDMethod;
    if ((getQueryByIDMethod = FaceRecognitionQueryServiceGrpc.getQueryByIDMethod) == null) {
      synchronized (FaceRecognitionQueryServiceGrpc.class) {
        if ((getQueryByIDMethod = FaceRecognitionQueryServiceGrpc.getQueryByIDMethod) == null) {
          FaceRecognitionQueryServiceGrpc.getQueryByIDMethod = getQueryByIDMethod = 
              io.grpc.MethodDescriptor.<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam, mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "face_rkg.FaceRecognitionQueryService", "QueryByID"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new FaceRecognitionQueryServiceMethodDescriptorSupplier("QueryByID"))
                  .build();
          }
        }
     }
     return getQueryByIDMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static FaceRecognitionQueryServiceStub newStub(io.grpc.Channel channel) {
    return new FaceRecognitionQueryServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static FaceRecognitionQueryServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new FaceRecognitionQueryServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static FaceRecognitionQueryServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new FaceRecognitionQueryServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class FaceRecognitionQueryServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void queryByID(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getQueryByIDMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getQueryByIDMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam,
                mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse>(
                  this, METHODID_QUERY_BY_ID)))
          .build();
    }
  }

  /**
   */
  public static final class FaceRecognitionQueryServiceStub extends io.grpc.stub.AbstractStub<FaceRecognitionQueryServiceStub> {
    private FaceRecognitionQueryServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionQueryServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionQueryServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionQueryServiceStub(channel, callOptions);
    }

    /**
     */
    public void queryByID(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam request,
        io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getQueryByIDMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class FaceRecognitionQueryServiceBlockingStub extends io.grpc.stub.AbstractStub<FaceRecognitionQueryServiceBlockingStub> {
    private FaceRecognitionQueryServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionQueryServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionQueryServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionQueryServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse queryByID(mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam request) {
      return blockingUnaryCall(
          getChannel(), getQueryByIDMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class FaceRecognitionQueryServiceFutureStub extends io.grpc.stub.AbstractStub<FaceRecognitionQueryServiceFutureStub> {
    private FaceRecognitionQueryServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private FaceRecognitionQueryServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceRecognitionQueryServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new FaceRecognitionQueryServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse> queryByID(
        mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam request) {
      return futureUnaryCall(
          getChannel().newCall(getQueryByIDMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_QUERY_BY_ID = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final FaceRecognitionQueryServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(FaceRecognitionQueryServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_QUERY_BY_ID:
          serviceImpl.queryByID((mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.IDParam) request,
              (io.grpc.stub.StreamObserver<mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.QueryResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class FaceRecognitionQueryServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    FaceRecognitionQueryServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return mesh.cpn.idl.internal_idl.generated.ai.face_rkg.FaceRkgQuery.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("FaceRecognitionQueryService");
    }
  }

  private static final class FaceRecognitionQueryServiceFileDescriptorSupplier
      extends FaceRecognitionQueryServiceBaseDescriptorSupplier {
    FaceRecognitionQueryServiceFileDescriptorSupplier() {}
  }

  private static final class FaceRecognitionQueryServiceMethodDescriptorSupplier
      extends FaceRecognitionQueryServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    FaceRecognitionQueryServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (FaceRecognitionQueryServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new FaceRecognitionQueryServiceFileDescriptorSupplier())
              .addMethod(getQueryByIDMethod())
              .build();
        }
      }
    }
    return result;
  }
}
